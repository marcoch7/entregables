# Tarea 1: Predictores de saltos

## Dependency

In order to run the program, gunzip is required to be installed.

## Compile

In order to compile, type in console:

```c
make exe
```

## Program description

This program reads the '.gz' file as input from stdin and does a branch prediction for the instructions in each line, these predictions can be done with 4 different predictors and have user input parameters such as Branch History table size, Global history register size, Private history register size, number of bits taken from PC as well as predictor type. It also prints in a '.txt' the first 5000 predictions if the user chooses a value of 1 for the '-o' parameter.

## Run program

To run the program, type in console:

```c
gunzip -c branch-trace-gcc.trace.gz | ./branch -s # -bp # -gh # -ph # -o #
```
Where '#' is the user input.

## Additional information

### bp

When running the program, 'bp' parameter can take numbers 1-4, each number will represent a predictor type as follows:

1. Bimodal
2. Global
3. Private
4. Tournament

### Example

```c
gunzip -c branch-trace-gcc.trace.gz | ./branch -s 4 -bp 4 -gh 3 -ph 7 -o 1
```
Typing this in console will return the Tournament prediction with a Global history register size of 3, a Private history register size of 7 and a Branch History table of size 2^4.
### Commentaries

-The '.h' files are located in the 'include' folder, these files have a detailed description for each function and structure.

-The '.c' files are located in the 'src' folder, these files have a brief description of the logic for each function.

