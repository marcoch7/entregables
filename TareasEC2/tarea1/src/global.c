#include "global.h"

// Function that returns the BHT index 
int global_BHT_index(int s, unsigned int PC, unsigned int old_index){
    unsigned int max_val = 0xffffffff;                              // 32 numbers 1 
    unsigned int mask_g = max_val >> (32 - s);                      // mask_g has 32-s numbers 1
    unsigned int indexone = PC ^ old_index;                         // indexone has the BHT index after performing the xor between the global register and PC
    unsigned int indexfinal = mask_g & indexone;                    // adjusts the size of indexone. indexfinal has the BHT index with the correct size
    return (int)indexfinal;
}

// Function that updates the global history register
unsigned int update_greg(char* outcome, unsigned int gh, unsigned int g_reg){
    unsigned int new_g_reg = g_reg << 1;                            // Shifts left in order to add new history to the register
    if (*outcome == 'T')                                            // If taken, new history is a 1, else it's a 0
    {
        new_g_reg++;
    }
    unsigned int max_val = 0xffffffff;                              // final_gregister has the new PHT register with a size of 32-gh
    unsigned int mask_g = max_val >> (32 - gh);
    unsigned int final_gregister = mask_g & new_g_reg;            
    return final_gregister;
}

// predict type function that has the global predictor logic
predict* global(unsigned int* g_reg, int gh, int s, unsigned int PC, unsigned int* BHT, char* outcome, results* resultP){
    unsigned int g_regint = 0;                                      // g_regint is the global history register
    g_regint = *g_reg;                                              // g_regint is given the value in global history register
    int index = global_BHT_index(s, PC, g_regint);                  // Gets the BHT index for the global history predictior
    predict* pointG = two_BC(BHT, index, outcome, resultP);         // Updates the two bit counters and stores results 
    unsigned int new_g_reg = update_greg(outcome, gh, g_regint);    // Updates the global history register    
    *g_reg = new_g_reg;                                             // Stores the updated global history register value
    return pointG;
}
