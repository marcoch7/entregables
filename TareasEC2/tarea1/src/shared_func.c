#include "shared_func.h"
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// results type function
// prediction: pointer with predict struct

results* results_f(){
    results* resultP = malloc(sizeof(results));
    resultP->predict_type = malloc(30*sizeof(char));
    resultP->BHT_size = 0;
    resultP->gh_size = 0;
    resultP->ph_size = 0;
    resultP->correct_tb = 0;
    resultP->incorrect_tb = 0;
    resultP->correct_nt = 0;
    resultP->incorrect_nt = 0;
    return resultP;
}

// predict type function
// prediction: pointer with predict struct

predict* prediction_f(){
    predict* prediction = malloc(sizeof(predict));                          // Allocates memory for predict struct attributes
    prediction->predicted = malloc(sizeof(char));
    prediction->predictedOut = malloc(30*sizeof(char));
    return prediction;
}

// Function that creates a BHT for each predictor

unsigned int* create_BHT(int s, results* resultP){
    double BHT_s = pow(2, (double)s);                                       // Number of entries for BHT
    unsigned int* BHT = malloc((unsigned int)BHT_s*sizeof(unsigned int));
    for (int i = 0; i < (unsigned int)BHT_s; i++){                          // 0 = SN, 1 = WN, 2 = WT, 3 = ST / 0 = SPshare, 1 = WPshare, 2 = WGshare, 3 = SGshare
        BHT[i] = 0;                                                         // BHT[i] has the unsigned int that represents a state for each register
    }
    resultP->BHT_size = (unsigned int)BHT_s;                                // Stores the size of BHT
    return BHT; 
}

// predict type function that contains the logic for a two bit counter. Where BHT[bindex] represents each register for the BHT

predict* two_BC(unsigned int* BHT, int bindex, char* outcome, results* resultP){
    predict* prediction = prediction_f();                                   // Call to function prediction_F() which allocates memory for the pointer prediction to store results
    int actual_out = BHT[bindex];
    if (*outcome == 'N'){
        if (actual_out == 0){ // SN->SN
            resultP->correct_nt++;
            prediction->predicted = "N";
            prediction->predictedOut =  "correct";
        }
        else if (actual_out == 1){
            BHT[bindex]--; // WN->SN
            resultP->correct_nt++;
            prediction->predicted = "N";
            prediction->predictedOut =  "correct";
        }
        else if (actual_out == 2){
            BHT[bindex]--; // WT->WN
            resultP->incorrect_tb++;
            prediction->predicted = "T";
            prediction->predictedOut =  "incorrect";
        }
        else if (actual_out == 3){
            BHT[bindex]--; // ST->WN
            resultP->incorrect_tb++;
            prediction->predicted = "T";
            prediction->predictedOut =  "incorrect";
        }
    }
    else if (*outcome == 'T'){
        if (actual_out == 0){ 
            BHT[bindex]++; // SN->WN
            resultP->incorrect_nt++;
            prediction->predicted = "N";
            prediction->predictedOut =  "incorrect";
        }
        else if (actual_out == 1){
            BHT[bindex]++; // WN->WT
            resultP->incorrect_nt++;
            prediction->predicted = "N";
            prediction->predictedOut =  "incorrect";
        }
        else if (actual_out == 2){
            BHT[bindex]++; // WT->ST
            resultP->correct_tb++;
            prediction->predicted = "T";
            prediction->predictedOut =  "correct";
        }
        else if (actual_out == 3){
            resultP->correct_tb++;
            prediction->predicted = "T";
            prediction->predictedOut =  "correct";
        }
    }
    return prediction;
}

// Function that when called updates the PHT register with the new history
unsigned int update_PHT(char* outcome, unsigned int PHT_point, int ph){
    unsigned int PHT_res = PHT_point << 1;                                  // Shifts left in order to add new history to the register
    if (*outcome == 'T'){                                                   // If taken, new history is a 1, else it's a 0
        PHT_res++;     
    }
    unsigned int max_value = 0xffffffff;                                    // ph_updated has the new PHT register with a size of 32-ph
    unsigned int mask = max_value >> (32 - ph);
    unsigned int ph_updated = mask & PHT_res;
    return ph_updated;
}

// Functions that print the results on console
void print_console(char* pred_type, int BHTsize, int globalRsize, int privateRsize, int nOfbranch, int correct_t, int incorrect_t, int correct_nt, int incorrect_nt){
    double percentage = 100*((double)correct_nt + (double)correct_t)/nOfbranch; 
    printf("------------------------------------------------------------------\n");
    printf("Prediction parameters:\n");
    printf("------------------------------------------------------------------\n");
    printf("Branch prediction type:                                %s\n", pred_type);
    printf("BHT size(entries):                                     %d\n", BHTsize); 
    printf("Global history register size:                          %d\n", globalRsize);
    printf("Private history register size:                         %d\n", privateRsize);
    printf("------------------------------------------------------------------\n");
    printf("Simulation Results:\n");
    printf("------------------------------------------------------------------\n");
    printf("Number of branch:                                      %d\n", nOfbranch);
    printf("Number of correct prediction of taken branches:        %d\n", correct_t);
    printf("Number of incorrect prediction of taken branches:      %d\n", incorrect_t);
    printf("Correct prediction of not taken branches:              %d\n", correct_nt);
    printf("Incorrect prediction of not taken branches:            %d\n", incorrect_nt);
    printf("Percentage of correct predictions:                     %f\n", percentage);
    printf("------------------------------------------------------------------\n");
}

// Functions that free allocated memory
void free_predictor(predict* prediction){
    free(prediction->predicted);
    free(prediction->predictedOut);
    free(prediction);
}
void free_results(results* resultP){
    free(resultP->predict_type);
    free(resultP);
}
void free_BHT(unsigned int* BHT){
    free(BHT);
}

