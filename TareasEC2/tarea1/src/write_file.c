#include "write_file.h"

void print_file(char* fname, int bp){
    FILE* fpointer = fopen(fname, "w");
    if (bp == 4)
    {
        fprintf(fpointer, "|       PC       |   Predictor   |  Outcome   | Prediction |  Correct/Incorrect  \n");
    }
    else
    {
        fprintf(fpointer, "|       PC       | Outcome  | Prediction |  Correct/Incorrect  \n");
    }
    fclose(fpointer);
}

void appendto_file(int bp ,unsigned int PC, char* fname, char* predictor, char* outcome, char* prediction, char* result){
    FILE* fpointer = fopen(fname, "a");
      if (bp == 4)
    {
        fprintf(fpointer, "|   %u   |     %s    |      %s     |     %s      |    %s     \n", PC, predictor, outcome, prediction, result);
    }
    else
    {
        fprintf(fpointer, "|   %u   |     %s    |      %s     |    %s     \n", PC, outcome, prediction, result);
    }
    fclose(fpointer);
}