#include "tournament.h"
#include <string.h>

// meta type function and a pointer to the struct attributes
meta* metapredictor(){
    meta* metaP = malloc(sizeof(meta));                                 // Allocate memory for struct attributes
    metaP->prediction = malloc(sizeof(char));
    metaP->predictor = malloc(sizeof(char));
    metaP->outcome = malloc(30*sizeof(char));

    return metaP;
}

// Function that returns the metapredictor index
unsigned int meta_index(int s, unsigned int PC){
    unsigned int max_val = 0xffffffff;                                  // 32 numbers 1 
    unsigned int mask_b = max_val >> (32 - s);                          // mask_b has 32-s numbers 1 
    unsigned int index = mask_b & PC;                                   // index has the 's' LSB from PC
    return (int)index;
}

// meta type function with the tournament predictior logic
meta* tournament(unsigned int* g_point, int gh, int ph, int s, unsigned int PC, unsigned int* BHT_g, unsigned int* BHT_p, unsigned int* BHT_t, unsigned int* PHT, char* outcome, results* resultP, results* resultsT){
    meta* pointM = metapredictor();                                     // pointM is a pointer to meta struct attributes
    int index = meta_index(s, PC);                                      // index contains the metapredictor index
    predict* pshare = private(ph, s, PC, BHT_p, PHT, outcome, resultP); // pshare calls private predictor
    predict* gshare = global(g_point, gh, s, PC, BHT_g, outcome, resultP);  // gshare calls global predictor
    int actual_out = BHT_t[index];                                      // actual_out has the value in each register of the tournament BHT, where 0 = Strongly pshare, 1 = Weakly pshare, 2 = Weakly gshare, 3 = Strongly gshare
    // tournament logic begins here
    if (strcmp(pshare->predicted, gshare->predicted) == 0)              // If both predicted the same, don't update the BHT register
    {   
        if(actual_out == 0 || actual_out == 1)                          // Chooses private if the register has a value of 0 or 1
        {
            pointM->predictor = "Pshare";
        }
        else if(actual_out == 2 || actual_out == 3)                     // Chooses global if the register has a value of 2 or 3
        {
            pointM->predictor = "Gshare";
        }
        pointM->prediction = pshare->predicted;                         // Since both predicted the same, store the prediction from pshare
        pointM->outcome = pshare->predictedOut;

        if(strcmp(pshare->predictedOut, "correct") == 0)                // If the prediction was correct
        {
            if(strcmp(pshare->predicted, "N") == 0)                     // If the prediction was a N, add 1 to correctly not taken
            {   
                resultsT->correct_nt++;                                   
            }
            else                                                        // If the prediction was a T, add 1 to correctly taken
            {
                resultsT->correct_tb++;
            }
        }
        else                                                            // If the prediction was incorrect
        {
            if(strcmp(pshare->predicted, "N") == 0)                     // If the prediction was a N, add 1 to incorrectly not taken
            {
                resultsT->incorrect_nt++;
            }
            else                                                        // If the prediction was a T, add 1 to incorrectly taken
            {
                resultsT->incorrect_tb++;
            }
        }
    }
    else if (strcmp(pshare->predicted, gshare->predicted) != 0)         // If they had different predictions
    {
        if (actual_out == 0)                                            // 0 = SPshare, 1 = WPshare, 2 = WGshare, 3 = SGshare
        {
            pointM->predictor = "Pshare";                               // If actual_out = 0, predict Pshare and store results                                             
            pointM->prediction = pshare->predicted;
            pointM->outcome = pshare->predictedOut;

            if(strcmp(pshare->predictedOut, "correct") == 0)            // If the prediction was correct
            {
                if(strcmp(pshare->predicted, "N") == 0)                 // If it was a N, add 1 to correctly not taken
                {
                    resultsT->correct_nt++;
                }
                else                                                    // If it was a T, add 1 to correctly taken
                {
                    resultsT->correct_tb++;
                }
            }
            else                                                        // If the prediction was incorrect
            {
                if(strcmp(pshare->predicted, "N") == 0)                 // If it was a N, add 1 to incorrectly not taken
                {
                    resultsT->incorrect_nt++;
                }
                else                                                    // If it was a T, add 1 to incorrectly taken
                {
                    resultsT->incorrect_tb++;
                }
            }

            if (*pshare->predicted != *outcome)                         // If the pshare prediction was incorrect, add 1 to the BHT register so it moves to weakly pshare
            {   
                BHT_t[index]++;
            }
            
        }
        else if (actual_out == 1)                                       // Weakly pshare 
        {
            pointM->predictor = "Pshare";                               // Store pshare results
            pointM->prediction = pshare->predicted;
            pointM->outcome = pshare->predictedOut;

            if(strcmp(pshare->predictedOut, "correct") == 0)            // If the prediction was correct        
            {
                if(strcmp(pshare->predicted, "N") == 0)                 // If it was a N, add 1 to correctly not taken
                {
                    resultsT->correct_nt++;
                }
                else                                                    // If it was a T, add 1 to correctly taken
                {
                    resultsT->correct_tb++;
                }
            }
            else                                                        // If the prediction was incorrect
            {
                if(strcmp(pshare->predicted, "N") == 0)                 // If it was a N, add 1 to incorrectly not taken
                {
                    resultsT->incorrect_nt++;
                }
                else                                                    // If it was a T, add 1 to incorrectly taken
                {
                    resultsT->incorrect_tb++;
                }
            }

            if (*pshare->predicted == *outcome)                         // If the pshare prediction was incorrect, add 1 to the BHT register so it moves to weakly gshare
            {
                BHT_t[index]--;
            }
            else                                                        // If the pshare prediction was correct, sub 1 to the BHT register so it moves to strongly pshare
            {
                BHT_t[index]++;
            }
            
        }
        else if (actual_out == 2)                                       // Weakly gshare
        {
            pointM->predictor = "Gshare";                               // Store gshare results
            pointM->prediction = gshare->predicted;
            pointM->outcome = gshare->predictedOut;

            if(strcmp(gshare->predictedOut, "correct") == 0)            // If the prediction was correct   
            {
                if(strcmp(gshare->predicted, "N") == 0)                 // If it was a N, add 1 to correctly not taken
                {
                    resultsT->correct_nt++;
                }
                else                                                    // If it was a T, add 1 to correctly taken
                {
                    resultsT->correct_tb++;
                }
            }
            else                                                        // If the prediction was incorrect
            {
                if(strcmp(gshare->predicted, "N") == 0)                 // If it was a N, add 1 to incorrectly not taken                    
                {
                    resultsT->incorrect_nt++;
                }
                else                                                    // If it was a T, add 1 to incorrectly taken
                {
                    resultsT->incorrect_tb++;
                }
            }

            if (*gshare->predicted == *outcome)                         // If the gshare prediction was correct, add 1 to the BHT register so it moves to strongly gshare
            {
                BHT_t[index]++;
            }
            else                                                        // If the gshare prediction was incorrect, sub 1 to the BHT register so it moves to weakly pshare
            {
                BHT_t[index]--;
            }
            
        }
        else if (actual_out == 3)                                       // Strongly gshare
        {
            pointM->predictor = "Gshare";                               // Store gshare results
            pointM->prediction = gshare->predicted;
            pointM->outcome = gshare->predictedOut;

            if(strcmp(gshare->predictedOut, "correct") == 0)            // If the prediction was correct 
            {
                if(strcmp(gshare->predicted, "N") == 0)                 // If it was a N, add 1 to correctly not taken
                {
                    resultsT->correct_nt++;
                }
                else                                                    // If it was a T, add 1 to correctly taken
                {
                    resultsT->correct_tb++;
                }
            }   
            else                                                        // If the prediction was incorrect
            {
                if(strcmp(gshare->predicted, "N") == 0)                 // If it was a N, add 1 to incorrectly not taken
                {
                    resultsT->incorrect_nt++;
                }
                else                                                    // If it was a T, add 1 to incorrectly taken
                {
                    resultsT->incorrect_tb++;
                }
            }

            if (*gshare->predicted != *outcome)                         // If the gshare prediction was incorrect, sub 1 to the BHT register so it moves to weakly gshare
            {
                BHT_t[index]--;
            }
        }
        
    } 
    return pointM;
}

