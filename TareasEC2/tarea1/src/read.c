#include "read.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// readfile type function
// readit: pointer with readfile struct
readfile* read_f(){ 
    readfile* readit = malloc(sizeof(readfile));                 // Allocates memory for pointer readit
    readit->instrPC = 0;                                         // Struct defined attributes
    readit->outcome = malloc(30*sizeof(char)); 
    return readit;
}

// Function that reads the file. Receives parameters from main
void parse_f(FILE* file, int s, int bp, int gh, int ph, int o){  

    char lineP[200];                                             // In order to read file
    if(file == NULL)                                             // If there is no file input, exits
    {
        printf("No file input");
        exit(1);
    }
    
    else
    {                                                           
        results* resultP = results_f();                            // resultP stores variables with results* struct, get_result() can be found at shared_func.c
        results* resultsT = results_f();                           // resultsT stores variables with results* struct, used in tournament predictor

        if (bp == 1)                                               // Prediction type is stored in resultP->predict_type
        {
            resultP->predict_type = "Bimodal";
        }
        else if (bp == 2)
        {
            resultP->predict_type = "Global";
            resultP->gh_size = gh;
        }
        else if (bp == 3)
        {
            resultP->predict_type = "Private";
            resultP->ph_size = ph;
        }
        else if (bp == 4)
        {
            resultP->predict_type = "Tournament";
        }

        unsigned int* BHT = create_BHT(s, resultP);                 // Stores BHT[i] for bimodal predictor, create_BHT() can be found at shared_func.c
        unsigned int* BHT_p = create_BHT(s, resultP);               // Stores BHT[i] for private predictor
        unsigned int* PHT = create_PHT(s);                          // Stores PHT[i] for private predictor
        unsigned int* BHT_g = create_BHT(s, resultP);               // Stores BHT[i] for global predictor
        unsigned int* BHT_t = create_BHT(s, resultP);               // Stores BHT[i] for tournament predictor
        unsigned int g_reg = 0;                                     // Global history register    
        unsigned int* g_point = &g_reg;                             // Pointer to global history register, updates this register
        char* fname = malloc(25*sizeof(char));                      // fname will have the prediction type name in order to create the .txt with "fname"
        int line_counter = 0;                                       // Is equal to the number of branches

        if (o == 1)                                                 // If o = 1, create the .txt file 
        {

            fname = strcat(strcat(fname, resultP->predict_type), ".txt");   // strcat allows me to create the ".txt" file while ammending the predict type name to a char* fname 
            print_file(fname, bp);
           
        }

           
        while(fgets(lineP, 200, file)!= NULL){               // Reads the file line by line

            line_counter++;
            readfile* readit = read_line(lineP);             // Stores the values of instrPC and outcome using read_line() function (found below) and pointer readit
        
            if (bp == 1)                                            // If bimodal
            {
                // predict struct type pointer that stores the bimodal() function return values. This struct can be found in shared_func.h
                predict* prediction = bimodal(s, readit->instrPC, BHT, readit->outcome, resultP);  // bimodal() function can be found in bimodal.c      
                if (o == 1 && line_counter < 5000){                 // Prints the lines for the first 5000 branches in the .txt file if o = 1

                    // Function that writes 1 line of the file. Can be found in write_file.c
                    appendto_file(bp, readit->instrPC, fname, resultP->predict_type, readit->outcome, prediction->predicted, prediction->predictedOut);
                    
                }
            }
            else if (bp == 2)                                       // If global
            { 
               // predict struct type pointer that stores the global() function return values. This struct can be found in shared_func.h
                predict* prediction = global(g_point, gh, s, readit->instrPC, BHT_g, readit->outcome, resultP); // global() function can be found in global.c
                if(o == 1 && line_counter < 5000){                  // Prints the lines for the first 5000 branches in the .txt file if o = 1

                    // Function that writes 1 line of the file. Can be found in write_file.c
                    appendto_file(bp, readit->instrPC, fname, resultP->predict_type, readit->outcome, prediction->predicted, prediction->predictedOut);
                    
                }
            }
            else if (bp == 3)                                       // If private
            {   
                // predict struct type pointer that stores the private() function return values. This struct can be found in shared_func.h
                predict* prediction = private(ph, s, readit->instrPC, BHT_p, PHT, readit->outcome, resultP); 
                if(o == 1 && line_counter < 5000){                  // Prints the lines for the first 5000 branches in the .txt file if o = 1

                     // Function that writes 1 line of the file. Can be found in write_file.c
                    appendto_file(bp, readit->instrPC, fname, resultP->predict_type, readit->outcome, prediction->predicted, prediction->predictedOut);
                    
                }
                
            }
            else if (bp == 4)                                       // If tournament
            {  
                // predict struct type pointer that stores the tournament() function return values. This struct can be found in shared_func.h
                meta* metaP = tournament(g_point, gh, ph, s, readit->instrPC, BHT_g, BHT_p, BHT_t, PHT, readit->outcome, resultP, resultsT);
                if(o == 1 && line_counter < 5000){                  // Prints the lines for the first 5000 branches in the .txt file if o = 1

                     // Function that writes 1 line of the file. Can be found in write_file.c
                    appendto_file(bp, readit->instrPC, fname, metaP->predictor, readit->outcome, metaP->prediction, metaP->outcome);
                }   
            }
            // Frees memory from pointer readit
            delete_read(readit);

        }
        // These "ifs" free memory and sends the stored values, calculated with each predictor's function, to the print_console() function, which can be found in shared_func.c
        if (bp == 1){
            free_BHT(BHT);
            print_console(resultP->predict_type, resultP->BHT_size, gh, ph, line_counter, resultP->correct_tb, resultP->incorrect_tb, resultP->correct_nt, resultP->incorrect_nt);
        }
        else if (bp == 2){
            free_BHT(BHT_g);
            print_console(resultP->predict_type, resultP->BHT_size, gh, ph, line_counter, resultP->correct_tb, resultP->incorrect_tb, resultP->correct_nt, resultP->incorrect_nt);        
        }
        else if (bp == 3){
            free_PHT(PHT);
            free_BHT(BHT_p);
            print_console(resultP->predict_type, resultP->BHT_size, gh, ph, line_counter, resultP->correct_tb, resultP->incorrect_tb, resultP->correct_nt, resultP->incorrect_nt);
        }
        else if (bp == 4){
            free_PHT(PHT);
            free_BHT(BHT_p);
            free_BHT(BHT_g);
            free_BHT(BHT_t);
            print_console(resultP->predict_type, resultP->BHT_size, gh, ph, line_counter, resultsT->correct_tb, resultsT->incorrect_tb, resultsT->correct_nt, resultsT->incorrect_nt);
        }
        // Frees memory from pointer fname
        free(fname);
    }
    fclose(file);
}

// readfile type function that reads one line of the file and stores PC and outcome
readfile* read_line(char* linereadp){ 
    readfile* readit = read_f();
    sscanf(linereadp, "%u %s", &readit->instrPC, readit->outcome);
    return readit;
}

// Function that frees readit pointer's allocated memory
void delete_read(readfile* readit){
    free(readit->outcome);
    free(readit);
}