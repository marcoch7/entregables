#include "private.h"

// Function that creates a PHT, this PHT has the same size as BHT
unsigned int* create_PHT(int s){
    double PHT_s = pow(2, (double)s);                       // Number of entries for PHT
    unsigned int* PHT = malloc((unsigned int)PHT_s*sizeof(unsigned int));
    for (int i = 0; i < (unsigned int)PHT_s; i++){          // Each number represents a state: 0 = SN, 1 = WN, 2 = WT, 3 = ST
        PHT[i] = 0;                                         //PHT[i] has the unsigned int that represents a state for each register
    }
    return PHT;
}

// Function that returns the PHT index 
int PHT_index(int s, unsigned int PC){
    unsigned int max_val = 0xffffffff;                      // 32 numbers 1 in order to perform AND with 32-s to get that number of bits from PC
    unsigned int mask_p = max_val >> (32 - s);              // mask_p has 32-s numbers 1
    unsigned int index = mask_p & PC;                       // index has the 's' LSB from PC
    return (int)index;
    
}

// Function that returns the BHT index for the private history predictor
int private_BHT_index(int s, unsigned int PC, unsigned int old_index){
    unsigned int max_val = 0xffffffff;                      // 32 numbers 1
    unsigned int mask_p = max_val >> (32 - s);              // mask_p has 32-s numbers 1
    unsigned int indexone = PC ^ old_index;                 // indexone has the BHT index after performing the xor between PHT register and PC
    unsigned int indexfinal = mask_p & indexone;            // adjusts the size of indexone. indexfinal has the BHT index with the correct size
    return (int)indexfinal;
}

// predict type function that has the private predictor logic
predict* private(int ph, int s, unsigned int PC, unsigned int* BHT, unsigned int* PHT, char* outcome, results* resultP){
    int index = PHT_index(s, PC);                           // Gets the PHT index
    unsigned int PHT_point = PHT[index];                    // PHT_point is a register from the PHT
    int indexfinal = private_BHT_index(s, PC, PHT_point);   // Gets the BHT index for the private predictor
    predict* pointPrivate = two_BC(BHT, indexfinal, outcome, resultP);  // Updates the two bit counters and stores results
    PHT[index] = update_PHT(outcome, PHT_point, ph);        // Updates the PHT registers' history
    return pointPrivate;
}

// Function that frees allocated memory from PHT
void free_PHT(unsigned int* PHT){
    free(PHT);
}