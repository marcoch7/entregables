#include "bimodal.h"

// Function that returns the BHT index for bimodal predictor
int get_index_bimodal(int s, unsigned int PC){
    unsigned int max_val = 0xffffffff;                      // 32 numbers 1 in order to perform xor with 32-s to get that number of bits from PC
    unsigned int mask_b = max_val >> (32 - s);              // mask_b has 32-s numbers 1
    unsigned int index = mask_b & PC;                       // index has the 's' LSB from PC
    return (int)index;
}

// predict type function that has the bimodal predictor logic
predict* bimodal(int s, unsigned int PC, unsigned int* BHT, char* outcome, results* resultP){
    int index = get_index_bimodal(s, PC);                   // Gets the BHT index
    predict* pointBimodal = two_BC(BHT, index, outcome, resultP); // Updates two bit counters and stores results
    return pointBimodal;
}