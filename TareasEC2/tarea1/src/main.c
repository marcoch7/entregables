#include "read.h"
#include "bimodal.h"
#include "global.h"
#include "private.h"
#include "shared_func.h"
#include "tournament.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

int main(int argc, char const *argv[]){
    
    unsigned int s, bp, ph, gh, o;
    for(size_t i = 1; i < argc; i++){           // reads from stdin
        if(strcmp(argv[i], "-s") == 0){         // when argv[i] = keyword
            s = atoi(argv[i+1]);                // store the parameter (located at argv[i+1]) for each keyword
        }
        else if(strcmp(argv[i], "-bp") == 0){
            bp = atoi(argv[i+1]);  
        }
        else if(strcmp(argv[i], "-ph") == 0){
            ph = atoi(argv[i+1]);
        }
        else if(strcmp(argv[i], "-gh") == 0){
            gh = atoi(argv[i+1]);   
        }
        else if(strcmp(argv[i], "-o") == 0){
            o = atoi(argv[i+1]); 
        }
    }
    parse_f(stdin, s, bp, gh, ph, o);        // jumps to this function, located at read.c
    return 0;
}