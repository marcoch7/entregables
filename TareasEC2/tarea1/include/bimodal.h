#ifndef _BIMODAL_H_
#define _BIMODAL_H_
#include "shared_func.h"

// Function that returns the BHT index for bimodal predictor
// s: input "s"
// PC: Program counter read from input file
int get_index_bimodal(int s, unsigned int PC);

// predict type function that has the bimodal predictor logic, returns the bimodal prediction for each line read
// s: input "s" read from console
// PC: Program counter read from input file
// BHT: Branch History Table
// outcome: Prediction outcome read from input file
// resultP: Pointer that stores the amount of correctly & incorrectly taken & not taken branches
predict* bimodal(int s, unsigned int PC, unsigned int* BHT, char* outcome, results* resultP);

#endif
