#ifndef _TOURNAMENT_H_
#define _TOURNAMENT_H_
#include "shared_func.h"
#include "private.h"
#include "global.h"

// Struct created to store results for the tournament branch predictor
// predictor: Chosen predictor
// prediction: Chosen prediction
// outcome: correct/incorrect
typedef struct 
{
    char* predictor;
    char* prediction;
    char* outcome;
    
}meta;

// Creates a meta type function and a pointer to the struct attributes, returns this pointer
meta* metapredictor();

// Function that returns the metapredictor index
// s: input "s" read from console
// PC: program counter, read from input file
unsigned int meta_index(int s, unsigned int PC);

// meta type function with the tournament predictior logic, returns pointer pointM, which has the chosen predictor for each prediction, the prediction and the outcome of the prediction
// g_point: pointer to the global history register
// gh: input "gh" read from console
// ph: input "ph" read from console
// s: input "s" read from console
// PC: program counter, read from input file
// BHT_g: Branch History Table for global predictor
// BHT_p: Branch History Table for private predictor
// BHT_m: Branch History Table for tournament predictor
// PHT: Private History Table
// outcome: outcome, read from input file
// resultP: pointer to results struct. Stores some results
// resultsT: pointer to results strcut. Stores tournament predictor results
meta* tournament(unsigned int* g_point, int gh, int ph, int s, unsigned int PC, unsigned int* BHT_g, unsigned int* BHT_p, unsigned int* BHT_m, unsigned int* PHT, char* outcome, results* resultP, results* resultsT);


#endif
