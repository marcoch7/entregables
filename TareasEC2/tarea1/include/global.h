#ifndef _GLOBAL_H_
#define _GLOBAL_H_
#include "shared_func.h"

// Function that returns the BHT index for the global predictor
// s: input "s"
// PC: Program counter read from input file
// old_index: current global history register index
int global_BHT_index(int s, unsigned int PC, unsigned int old_index);

// Function that updates the global history register, returns the updated global history register
// outcome: Prediction outcome read from input file
// gh: input "gh" read from console
// g_reg: global history register
unsigned int update_greg(char* outcome, unsigned int gh, unsigned int g_reg);

// predict type function that has the global predictor logic, returns the global prediction for each line read
// g_reg: pointer to the global history register
// gh: input "gh" read from console
// s: input "s" read from console
// PC: Program counter read from input file
// BHT: Branch History Table
// outcome: Prediction outcome read from input file
// resultP: Pointer that stores the amount of correctly & incorrectly taken & not taken branches
predict* global(unsigned int* g_reg, int gh, int s, unsigned int PC, unsigned int* BHT, char* outcome, results* resultP);


#endif
