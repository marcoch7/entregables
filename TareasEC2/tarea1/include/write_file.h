#ifndef _WRITE_F_
#define _WRITE_F_
#include <stdio.h>
#include <stdlib.h>


// Function that prints a file if the parameter '-o' has a value of 1
// filename: pointer that has the predictor type name in order to name the '.txt'
// bp: parameter 'bp' read from console, determines the name of the '.txt'
void print_file(char* filename, int bp);

// Function that appends the first 5000 prediction to the file
// bp: parameter 'bp' read from console, determines the name of the predictor chosen
// PC: Program counter read from input file
// fname: pointer that read that is used to open and write in the file
// predictor: pointer that has the predictor chosen stored
// outcome: pointer that has the outcome read from file	
// prediction: pointer that has the prediction chosen by the predictor
// result: pointer that has the outcome of the prediction made by the chosen predictor
void appendto_file(int bp ,unsigned int PC, char* fname, char* predictor, char* outcome, char* prediction, char* result);

#endif
