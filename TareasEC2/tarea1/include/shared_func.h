#ifndef _SHARED_F_
#define _SHARED_F_
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "write_file.h"

// Struct created to store results
// predict_type: prediction type
// gh_size: stores the size of the Global history register
// ph_size: stores the size of the Private history register
// correct_tb: stores the amount of correctly taken branches for bimodal, private and global predictors
// incorrect_tb: stores the amount of incorrectly taken branches for bimodal, private and global predictors
// correct_nt: stores the amount of correctly not taken branches for bimodal, private and global predictors
// incorrect_nt: stores the amount of incorrectly not taken branches for bimodal, private and global predictors
typedef struct 
{
    char* predict_type; // bimodal, private, global or tournament
    int BHT_size; 
    int gh_size;
    int ph_size;
    int correct_tb;
    int incorrect_tb;
    int correct_nt;
    int incorrect_nt;
  

}results;

// Struct created to store the bimodal, private and global predictor's prediction and prediction outcome
// predicted: stores the prediction
// predictedOut: stores the prediction outcome
typedef struct 
{
    char* predicted; 
    char* predictedOut; 

}predict;

// Creates a predict type function and a pointer to the struct attributes
// returns the pointer prediction, which has the predict struct attributes
predict* prediction_f(); 

// Creates a results type function and a pointer to the struct attributes
// returns the pointer resultsP, which has the results struct attributes
results* results_f(); 

// predict type two bit counter. This function contains the logic for the 2 bit counter, returns pointer prediction, which has the prediction and outcome for each line read
// BHT: Branch history table
// bindex: Branch history table index
// outcome: Prediction outcome read from input file
// resultP: Pointer that stores the amount of correctly & incorrectly taken & not taken branches
predict* two_BC(unsigned int* BHT, int bindex, char* outcome, results* resultP); 

// Function that creates a BHT
// s: parameter 's' read from input, indicates the amount of least significant bits taken from PC in order to index the BHT and gives the BHT size (2^s)
// resultP: pointer that has struct results attributes. Stores the BHT size
unsigned int* create_BHT(int s, results* resultP); 

// Function that updates each PHT register, returns ph_updated, which is the updated register "PHT_point"
// outcome: outcome for each prediction, read from input file
// PHT_point: register from PHT about to be updated in order to continue reading the PHT
// ph: parameter 'ph' read from stdin, gives the size of the PHT registers
unsigned int update_PHT(char* outcome, unsigned int PHT_point, int ph);

// Function that prints the results on console
// pred_type: Type of prediction
// BHTsize: size of the BHT table
// globalRsize: size of the global history register
// privateRsize: size of the private history registers
// nOfbranch: number of branches
// correct_t: number of correctly taken branches
// incorrect_t: number of incorrectly taken branches
// correct_nt: number of correctly not taken branches
// incorrect_nt: number of incorrectly not taken branches
void print_console(char* pred_type, int BHTsize, int globalRsize, int privateRsize, int nOfbranch, int correct_t, int incorrect_t, int correct_nt, int incorrect_nt);

// Functions that free allocated memory
void free_results(results* resultP);
void free_predictor(predict* prediction);
void free_BHT(unsigned int* BHT);


#endif

