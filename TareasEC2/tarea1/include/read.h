#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "shared_func.h"
#include "write_file.h"
#include "bimodal.h"
#include "private.h"
#include "global.h"
#include "tournament.h"

// Struct for trace
// instrPC: PC read from input file
// outcome: outcome read from input file
typedef struct 
{
    unsigned int instrPC;
    char* outcome;
}readfile;

// Creates a readFile type function, returns pointer readit, which has the attributes from struct readfile
// readit: pointer with readFile struct
readfile* read_f(); 

// Creates a readFile type function, returns the stored information in pointer readit, this information is read from stdin
// Reads input from each line with sscanf and stores it via pointer readit
readfile* read_line(char* linereadp);

// Function that reads the file. Receives parameters from main
// Predictor chosen with parameter bp
void parse_f(FILE* file, int s, int bp, int gh, int ph, int o);

// Function that frees memory for pointer readit
void delete_read(readfile* readit);
