#ifndef _PRIVATE_H_
#define _PRIVATE_H_
#include "shared_func.h"

// Function that creates a PHT, this PHT has the same size as BHT, returns the PHT
// s: input "s"
unsigned int* create_PHT(int s);

// Function that returns the PHT index 
// s: input "s"
// PC: Program counter read from input file
int PHT_index(int s, unsigned int PC);

// Function that returns the BHT index for the private history predictor
// s: input "s"
// PC: Program counter read from input file
// ph_updated: Private history register updated with new history 
int private_BHT_index(int s, unsigned int PC, unsigned int ph_updated);

// predict type function that has the private predictor logic, returns the private prediction for each line read
// ph: input "ph" read from console
// s: input "s" read from console
// PC: Program counter read from input file
// BHT: Branch History Table
// PHT: Private History Table
// outcome: Prediction outcome read from input file
// resultP: Pointer that stores the amount of correctly & incorrectly taken & not taken branches
predict* private(int ph, int s, unsigned int PC, unsigned int* BHT, unsigned int* PHT, char* outcome, results* resultP);

// Function that frees allocated memory from PHT
void free_PHT(unsigned int* PHT);
#endif
